package com.company;

public class Main {

    public static void main(String[] args) {
        String a = "hello";
        String b = "hello";
        String c = new String("hello");


        System.out.println(a == b);
        System.out.println(a == c);

        testMemory("abc", true);
        testMemory("abc", true);
        testMemory("abc", true);
        testMemory("abc", true);
        testMemory("abc", true);
        testMemory("abc", true);
        testMemory("abc", true);
        testMemory("abce", true);
        testMemory("abcb", true);
        System.out.println("amtf");
        System.out.println(String.format("总内存: %s", humanReadableByteCount(Runtime.getRuntime().totalMemory(), true)));
    }

    public static void testMemory(String name, boolean isUsed) {
        System.out.println("是否使用轻量级：" + isUsed);
        countMemory();
        System.out.println("=================");
    }

    public static void countMemory() {
        Runtime.getRuntime().gc();
        long used = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println(String.format("已使用内存: - %s - %s", used, humanReadableByteCount(used, true)));
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }
}
